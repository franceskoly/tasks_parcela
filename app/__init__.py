from flask import Flask
from .views import page # importamos la instancia de todas las rutas almacenadas en page
from flask_wtf.csrf import CSRFProtect # importamos la clase CSRFProtect

from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy



app = Flask(__name__)

# Instancias creadas
db = SQLAlchemy()
csrf = CSRFProtect()
bootstrap=Bootstrap()

from .models import User # importamos la clase User (modelos)

def create_app(config):
    app.config.from_object(config)

    # se asocia las instancias creadas
    csrf.init_app(app)
    bootstrap.init_app(app)
    app.register_blueprint(page) # indicamos al servidor con que rutas trabajar

    with app.app_context():
        db.init_app(app)
        db.create_all()

    return app