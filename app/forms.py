from wtforms import Form
from wtforms import validators # valida las entradas a los input
from wtforms import StringField, PasswordField, EmailField, BooleanField

class LoginForm(Form): # Clase que permite hacer formulario de Login
    username = StringField('Username', [validators.length(min=5, max=50, message="El Username se encuentra fuera de rango"), 
                                        validators.DataRequired()])
    password = PasswordField('Password', [validators.DataRequired(message="El password es requerido")])

class RegisterForm(Form):
    username = StringField('Username',[validators.length(min=5, max=60)])
    email = EmailField('Correo Electrónico', [
        validators.length(min=5, max=90),
        validators.DataRequired(message="El email es requerido."),
        validators.Email(message="Ingrese un email válido")
    ])
    password = PasswordField('New Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message="Las contraseñas deben coincidir")
    ])
    confirm = PasswordField('Confirmar Password')
    accept = BooleanField('I accept the TOS', [
        validators.DataRequired()
    ])