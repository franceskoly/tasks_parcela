class Config:
    SECRET_KEY = "@0823Noacanoa"  # generamos la llave secreta para el uso de csrf

class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI="postgresql://postgres:noacanoa@localhost:5432/tasks_parcela"
    #SQLALCHEMY_TRACK_MODIFICATIONS = False

config = {
    'development': DevelopmentConfig,
    'default': DevelopmentConfig
}